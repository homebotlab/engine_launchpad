//const mongoose = require('mongoose');
//const response = require('../../../helpers/response');
//const request = require('../../../helpers/request');
// const pagination = require('../../../helpers/pagination');
// const Event = mongoose.model('Event');

const configurator = require(`${process.env.HELPERS_PATH}/modules`);

exports.list = async function(req, res) {
    let permittedTiles = [];
    let data = {};

    const modules = configurator.getModules();

    modules.forEach(item => {
        if(item.tiles){
            item.tiles.forEach(tile => {            
                if(req.permission.attributes.indexOf(tile._id)>-1){
                    permittedTiles.push(tile);
                }
            })
        }
    })
    
    res.json({ tiles: permittedTiles, data });
};

// exports.read = function(req, res) {
//     Tile.findById(req.params.id, function(err, tile) {
//         if (err) return response.sendNotFound(res);
//         res.json(tile);
//     });
// };

// exports.create = function(req, res) {
//     const newTile = new Tile(req.body);
//     newTile.role = 'tile';
//     newTile.save(function(err, tile) {
//         if (err) return response.sendBadRequest(res, err);
//         response.sendCreated(res, tile);
//     });
// };

// exports.update = function(req, res) {
//     const tile = req.body;
//     delete tile.role;
//     Tile.findOneAndUpdate({ _id: req.params.id }, tile, { new: true, runValidators: true }, function(err, tile) {
//         if (err) return response.sendBadRequest(res, err);
//         res.json(tile);
//     });
// };

// exports.delete = function(req, res) {
//     Tile.remove({ _id: req.params.id }, function(err, tile) {
//         if (err) return res.send(err);
//         res.json({ message: 'Tile successfully deleted' });
//     });
// };