sap.ui.define(
  [
    'sap/ui/unified/Shell',
    'sap/ui/unified/ShellHeadItem',
    'sap/ui/unified/ShellHeadUserItem',
    'engine/launchpad/model/formatter',
    'sap/m/SearchField',
    'sap/m/MessagePopover',

  ],
  function (UnifiedShell, ShellHeadItem, ShellHeadUserItem, formatter, SearchField) {
    'use strict'

    return UnifiedShell.extend('engine.launchpad.common.Shell', {
      formatter: formatter,

      init: function () {
        UnifiedShell.prototype.init.apply(this, arguments);
        var oModel = new sap.ui.model.json.JSONModel({
          profile:{
            notifications: []
          }
        });

        var self = this;

        this.setModel(oModel, "service");
        // io('/').on('notification', function (data) {
        //   console.log(data);

        //   if(data.receivers.some(function(receiver){ return oModel.getProperty("/profile/_id") === receiver})) {
        //     //pushing new notification to the model
        //     self._oMessageButton.setSelected(true);

        //     oModel.setProperty("/profile/notifications/" + oModel.getProperty("/profile/notifications").length, {
        //       type: data.type,
        //       title: data.title,
        //       description: data.description,
        //       dttm: data.dttm
        //     });
        //   }
        // });

        // var oModel = new sap.ui.model.json.JSONModel();
        // oModel.setData(aMessages);

        // this.addHeadItem(
        //  new ShellHeadItem({
        //      tooltip:"Configuration",
        //      icon:"sap-icon://menu2",
        //      press: this.onPressConfiguration
        //  })
        // );

        this.addHeadItem(
          new ShellHeadItem({
            tooltip: 'Home',
            icon: 'sap-icon://home',
            press: this.onPressHome
          })
        )

        this._oMessageButton = new ShellHeadItem({
          tooltip: 'Messages',
          icon: 'sap-icon://bell',
          toggleEnabled: true,
          press: this.openInvitesPopover.bind(this)
        });

        this.addHeadEndItem(this._oMessageButton);

        this.setUser(
          new ShellHeadUserItem({
            image: 'sap-icon://person-placeholder',
            username: 'Undefined',
            press: this.onUserItemPressed.bind(this)
          })
        )

        // this.setSearch(
        //  new SearchField({
        //      search: this.onSearchPressed
        //  })
        // );

        // this.addPaneContent({
        //  <Text text="Lorem ipsum" />
        // });

        this._oResponsivePopover = new sap.m.ResponsivePopover({
            placement: "Bottom",
            showHeader: false,
            content: new sap.m.List({
                items:[
                    new sap.m.StandardListItem({
                        icon: "sap-icon://account",
                        title: "Мой профиль",
                        type: sap.m.ListType.Active,
                        press: this.onPressProfile.bind(this)
                    }),
                    new sap.m.StandardListItem({
                        icon: "sap-icon://log",
                        title: "Выход",
                        type: sap.m.ListType.Active,
                        press: this.onPressLogoff.bind(this)
                    })
                ]
            })
        });

        this._loadUserData()
      },

      onPressConfiguration: function () {},

      onPressHome: function () {
        window.location = '/modules/launchpad/webapp/index.html'
      },

      onPressProfile: function () {
        window.location = '/modules/profile/webapp/index.html#/UserSet/' + this.getModel("service").getProperty("/profile/_id");
      },

      onPressLogoff: function() {
          $.getJSON(
              '/api/v0.1/auth/logout',
              function(response) {
                  window.location = '/'
              }
          ).always(function() {
            window.location = '/'
          });
      },

      onUserItemPressed: function (oEvent) {
        this._oResponsivePopover.openBy(oEvent.getSource());
      },

      _loadUserData: function () {
        $.getJSON(
          '/api/v0.1/auth/profile',
          function (response) {
            response.notifications = response.notifications || [];
            this.getModel("service").setProperty("/profile", response);
            this.getUser().setUsername(String.prototype.concat.call(response.firstname || "", " " ,response.lastname || ""));
          }.bind(this)
        )
      },

      openInvitesPopover: function (oEvent) {
        this._oMessageButton.setSelected(false);
        if (!this._invPopover) {
          this._invPopover = new sap.m.MessagePopover({
            items: {
              path: '/profile/notifications',
              template: new sap.m.MessagePopoverItem({
                type: '{type}',
                title: '{title}',
                subtitle: {
                  parts: ['dttm'],
                  formatter: formatter.dateFormatString
                },
                description: '{description}'
              })
            }
          }).setModel(this.getModel("service"));
        }
        this._invPopover.toggle(oEvent.getSource());
      }
    })
  },
  /* bExport= */
  true
)