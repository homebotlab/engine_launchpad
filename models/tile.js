const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;
const TileSchema = new Schema({
    url: { type: String },
    icon: { type: String },
    title: { type: String }
});

TileSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Tile', TileSchema);