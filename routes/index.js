const express = require('express');

const aclMiddleware = require(`${process.env.MIDDLEWARES_PATH}/permission`);
const authMiddleware = require(`${process.env.MIDDLEWARES_PATH}/authentication`);

const tiles = require('../controllers/tiles');

const routes  = express.Router();

routes.route('/')
  .get(authMiddleware(), aclMiddleware.readAny('launchpad'), tiles.list);


module.exports = routes;
